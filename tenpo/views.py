from django.views.generic import TemplateView,ListView
from .models import Tenpo,Rireki,Favorite

class TopPageView(TemplateView):
    template_name = 'tenpo/index.html'

class TenpoListView(ListView):
	model = Tenpo
	queryset = Tenpo.objects.all()
	template_name = 'tenpo/tenpo.html'

class RirekiListView(ListView):
	model = Rireki
	queryset = Rireki.objects.all()
	template_name = 'tenpo/rireki.html'

class FavoriteListView(ListView):
	model = Favorite
	queryset = Favorite.objects.all()
	template_name = 'tenpo/favorite.html'
