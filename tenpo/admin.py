from django.contrib import admin
from .models import Tenpo,Rireki,Favorite

class TenpoAdmin(admin.ModelAdmin):
	fieldsets = [
        (None,{'fields':['tenpo_name','tenpo_genre','tenpo_address','tenpo_phone_number','tenpo_pr_message']})
	]
	list_display = ['id','tenpo_name','tenpo_genre','tenpo_address','tenpo_phone_number','tenpo_pr_message']

class RirekiAdmin(admin.ModelAdmin):
	fieldsets = [
        (None,{'fields':['rireki_tenpo','rireki_user']})
	]
	list_display = ['id','rireki_tenpo','rireki_user']

class FavoriteAdmin(admin.ModelAdmin):
	fieldsets = [
        (None,{'fields':['favorite_tenpo','favorite_user']})
	]
	list_display = ['id','favorite_tenpo','favorite_user']

admin.site.register(Tenpo,TenpoAdmin)
admin.site.register(Rireki,RirekiAdmin)
admin.site.register(Favorite,FavoriteAdmin)
