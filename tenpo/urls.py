from django.conf.urls import url
from . import views

app_name = 'tenpo'
urlpatterns = [
    url(r'^$', views.TopPageView.as_view(), name='index'),
    url(r'^tenpo/$', views.TenpoListView.as_view(), name='tenpo'),
    url(r'^rireki/$', views.RirekiListView.as_view(), name='rireki'),
    url(r'^favorite/$', views.FavoriteListView.as_view(), name='favorite'),
]
