from django.db import models
from django.contrib.auth.models import User

class Tenpo(models.Model):
	tenpo_name = models.CharField(default='tenpo',max_length=200)
	tenpo_genre = models.CharField(default='genre',max_length=200)
	tenpo_address = models.CharField(default='address',max_length=500)
	tenpo_phone_number = models.CharField(default='0660123456',max_length=500)
	tenpo_pr_message = models.CharField(default='message',max_length=1000)

	def __str__(self):
		return self.tenpo_name

class Rireki(models.Model):
	rireki_tenpo = models.ForeignKey(Tenpo,on_delete=models.CASCADE)
	rireki_user = models.ForeignKey(User,on_delete=models.CASCADE)

	def __str__(self):
		return self.rireki_tenpo.tenpo_name

class Favorite(models.Model):
	favorite_tenpo = models.ForeignKey(Tenpo,on_delete=models.CASCADE)
	favorite_user = models.ForeignKey(User,on_delete=models.CASCADE)

	def __str__(self):
		return self.favorite_tenpo.tenpo_name

def genreTable():
	return (
		(1,'飲食店'),
		(2,'ショッピング'),
		(3,'ヘルス'),
		(4,'ホテル'),
		(5,'美容'),
		(6,'その他'),
	)
